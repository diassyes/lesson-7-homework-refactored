package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"lesson-7-homework-refactored/src/domain"
	rpcHTTP "lesson-7-homework-refactored/src/rpc/http"
	"lesson-7-homework-refactored/src/user"
	"os"
)

func main() {
	baseUrl := "https://jsonplaceholder.typicode.com"

	userQueryRepo := rpcHTTP.NewUserQueryRepo(baseUrl)
	postQueryRepo := rpcHTTP.NewPostQueryRepo(baseUrl)
	albumQueryRepo := rpcHTTP.NewAlbumQueryRepo(baseUrl)

	userService := user.NewService(
		userQueryRepo,
		postQueryRepo,
		albumQueryRepo,
	)

	userId := 3
	userData, err := userService.GetUser(userId)
	if err != nil {
		panic(err)
	}
	writeFile(userData, fmt.Sprintf("user-%d.json", userId))
}

func writeFile(userData *domain.User, fileName string) {
	userJson, err := json.MarshalIndent(userData, "", "    ")
	if err != nil {
		panic(err)
	}
	err = ioutil.WriteFile(fileName, userJson, os.ModePerm)
	if err != nil {
		panic(err)
	}
}
