package user

import (
	"lesson-7-homework-refactored/src/domain"
	"sync"
)

type IService interface {
	GetUser(int) (*domain.User, error)
	GetUserPosts(int) (*[]domain.Post, error)
	GetUserAlbums(int) (*[]domain.Album, error)
}

type Service struct {
	userQueryRepo domain.UserQueryRepo
	postQueryRepo domain.PostQueryRepo
	albumQueryRepo domain.AlbumQueryRepo
}

func NewService(userQueryRepo domain.UserQueryRepo, postQueryRepo domain.PostQueryRepo, albumQueryRepo domain.AlbumQueryRepo) IService {
	return &Service{
		userQueryRepo: userQueryRepo,
		postQueryRepo: postQueryRepo,
		albumQueryRepo: albumQueryRepo,
	}
}

func (service *Service) GetUser(userId int) (*domain.User, error) {

	user, err := service.userQueryRepo.GetUser(userId)
	if err != nil {
		return nil, err
	}

	wg := sync.WaitGroup{}

	// Receiving User Posts ---------------------------
	wg.Add(1)
	go func(wg *sync.WaitGroup, service *Service, user *domain.User) {
		defer wg.Done()
		userPostsPtr, err := service.GetUserPosts(userId)
		if err != nil {
			panic(err)
		}
		user.Posts = make([]domain.Post, len(*userPostsPtr))
		copy(user.Posts, *userPostsPtr)
	}(&wg, service, user)
	// ------------------------------------------------

	// Receiving User Albums --------------------------
	wg.Add(1)
	go func(wg *sync.WaitGroup, service *Service, user *domain.User) {
		defer wg.Done()
		userAlbumsPtr, err := service.GetUserAlbums(userId)
		if err != nil {
			panic(err)
		}
		user.Albums = make([]domain.Album, len(*userAlbumsPtr))
		copy(user.Albums, *userAlbumsPtr)
	}(&wg, service, user)
	// ------------------------------------------------

	// Receiving User Todos ---------------------------
	wg.Add(1)
	go func(wg *sync.WaitGroup, service *Service, user *domain.User) {
		defer wg.Done()
		userTodosPtr, err := service.userQueryRepo.GetUserTodos(userId)
		if err != nil {
			panic(err)
		}
		user.Todos = make([]domain.Todo, len(*userTodosPtr))
		copy(user.Todos, *userTodosPtr)
	}(&wg, service, user)
	// ------------------------------------------------

	wg.Wait()
	return user, nil
}

func (service *Service) GetUserPosts(userId int) (*[]domain.Post, error) {

	userPosts, err := service.userQueryRepo.GetUserPosts(userId)
	if err != nil {
		return nil, err
	}

	wg := sync.WaitGroup{}
	for i := 0; i < len(*userPosts); i++ {
		wg.Add(1)
		go func(wg *sync.WaitGroup, post *domain.Post) {
			defer wg.Done()
			commentsPtr, err := service.postQueryRepo.GetPostComments(i+1)
			if err != nil {
				panic(err)
			}
			post.Comments = make([]domain.Comment, len(*commentsPtr))
			copy(post.Comments, *commentsPtr)
		}(&wg, &(*userPosts)[i])
	}

	wg.Wait()

	return userPosts, nil
}

func (service *Service) GetUserAlbums(userId int) (*[]domain.Album, error) {

	userAlbums, err := service.userQueryRepo.GetUserAlbums(userId)
	if err != nil {
		return nil, err
	}

	wg := sync.WaitGroup{}
	for i := 0; i < len(*userAlbums); i++ {
		wg.Add(1)
		go func(wg *sync.WaitGroup, album *domain.Album) {
			defer wg.Done()
			photosPtr, err := service.albumQueryRepo.GetAlbumPhotos(i+1)
			if err != nil {
				panic(err)
			}
			album.Photos = make([]domain.Photo, len(*photosPtr))
			copy(album.Photos, *photosPtr)
		}(&wg, &(*userAlbums)[i])
	}

	wg.Wait()

	return userAlbums, nil
}
