package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"lesson-7-homework-refactored/src/domain"
	"net/http"
)

type postQueryRepo struct {
	postsUrl string
}

func NewPostQueryRepo(baseUrl string) domain.PostQueryRepo {
	return &postQueryRepo{postsUrl: baseUrl + "/posts"}
}

func(postQueryRepo *postQueryRepo) GetPostComments(postId int) (*[]domain.Comment, error) {
	var comments []domain.Comment
	response, err := http.Get(
		postQueryRepo.postsUrl +
		fmt.Sprintf("/%d/comments", postId),
	)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal([]byte(body), &comments)
	if err != nil {
		return nil, err
	}
	return &comments, err
}