package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"lesson-7-homework-refactored/src/domain"
	"net/http"
)

type userQueryRepo struct {
	usersUrl string
}

func NewUserQueryRepo(baseUrl string) domain.UserQueryRepo {
	return &userQueryRepo{usersUrl: baseUrl + "/users"}
}

func (userQueryRepo *userQueryRepo) GetUser(userId int) (*domain.User, error) {
	var user domain.User
	response, err := http.Get(
		userQueryRepo.usersUrl +
		fmt.Sprintf("/%d", userId),
	)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal([]byte(body), &user)
	if err != nil {
		return nil, err
	}
	return &user, err
}

func (userQueryRepo *userQueryRepo) GetUserPosts(userId int) (*[]domain.Post, error) {
	var posts []domain.Post
	response, err := http.Get(
		userQueryRepo.usersUrl +
		fmt.Sprintf("/%d/posts", userId),
	)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal([]byte(body), &posts)
	if err != nil {
		return nil, err
	}
	return &posts, err
}

func (userQueryRepo *userQueryRepo) GetUserAlbums(userId int) (*[]domain.Album, error) {
	var albums []domain.Album
	response, err := http.Get(
		userQueryRepo.usersUrl +
		fmt.Sprintf("/%d/albums", userId),
	)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal([]byte(body), &albums)
	if err != nil {
		return nil, err
	}
	return &albums, err
}

func (userQueryRepo *userQueryRepo) GetUserTodos(userId int) (*[]domain.Todo, error) {
	var todos []domain.Todo
	response, err := http.Get(
		userQueryRepo.usersUrl +
		fmt.Sprintf("/%d/todos", userId),
	)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal([]byte(body), &todos)
	if err != nil {
		return nil, err
	}
	return &todos, err
}
