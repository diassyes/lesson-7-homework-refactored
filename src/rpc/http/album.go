package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"lesson-7-homework-refactored/src/domain"
	"net/http"
)

type albumQueryRepo struct {
	albumsUrl string
}

func NewAlbumQueryRepo(baseUrl string) domain.AlbumQueryRepo {
	return &albumQueryRepo{albumsUrl: baseUrl + "/albums"}
}

func(albumQueryRepo *albumQueryRepo) GetAlbumPhotos(albumId int) (*[]domain.Photo, error) {
	var photos []domain.Photo
	response, err := http.Get(
		albumQueryRepo.albumsUrl +
		fmt.Sprintf("/%d/photos", albumId),
	)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal([]byte(body), &photos)
	if err != nil {
		return nil, err
	}
	return &photos, err
}