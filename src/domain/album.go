package domain

type Album struct {
	UserId int `json:"userId,omitempty"`
	Id     int `json:"id,omitempty"`
	Title  string `json:"title,omitempty"`

	Photos []Photo `json:"photos,omitempty"`
}

type Photo struct {
	AlbumId      int    `json:"albumId,omitempty"`
	Id           int    `json:"id,omitempty"`
	Title        string `json:"title,omitempty"`
	Url          string `json:"url,omitempty"`
	ThumbnailUrl string `json:"thumbnailUrl,omitempty"`
}

type AlbumQueryRepo interface {
	GetAlbumPhotos(int) (*[]Photo, error)
}
