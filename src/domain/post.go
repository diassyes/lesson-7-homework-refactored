package domain

type Post struct {
	UserId int    `json:"userId,omitempty"`
	Id     int    `json:"id,omitempty"`
	Title  string `json:"title,omitempty"`
	Body   string `json:"body,omitempty"`

	Comments []Comment `json:"comments,omitempty"`
}

type Comment struct {
	PostId int    `json:"postId,omitempty"`
	Id     int    `json:"id,omitempty"`
	Name   string `json:"name,omitempty"`
	Email  string `json:"email,omitempty"`
	Body   string `json:"body,omitempty"`
}

type PostQueryRepo interface {
	GetPostComments(int) (*[]Comment, error)
}