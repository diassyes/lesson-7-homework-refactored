package domain

type User struct {
	Id       int     `json:"id,omitempty"`
	Name     string  `json:"name,omitempty"`
	Username string  `json:"username,omitempty"`
	Email    string  `json:"email,omitempty"`
	Address  Address `json:"address,omitempty"`
	Phone    string  `json:"phone,omitempty"`
	Website  string  `json:"website,omitempty"`
	Company  Company `json:"company,omitempty"`

	Posts    []Post `json:"posts,omitempty"`
	Albums    []Album `json:"albums,omitempty"`
	Todos    []Todo `json:"todos,omitempty"`
}

type Todo struct {
	UserId    int    `json:"userId,omitempty"`
	Id        int    `json:"id,omitempty"`
	Title     string `json:"title,omitempty"`
	Completed bool   `json:"completed"`
}

type Address struct {
	Street  string `json:"street,omitempty"`
	Suite   string `json:"suite,omitempty"`
	City    string `json:"city,omitempty"`
	ZipCode string `json:"zipCode,omitempty"`
	Geo     Geo `json:"geo,omitempty"`
}

type Geo struct {
	Lat string `json:"lat,omitempty"`
	Lng string `json:"lng,omitempty"`
}

type Company struct {
	Name        string `json:"name,omitempty"`
	CatchPhrase string `json:"catchPhrase,omitempty"`
	Bs          string `json:"bs,omitempty"`
}

type UserQueryRepo interface {
	GetUser(int) (*User, error)
	GetUserPosts(int) (*[]Post, error)
	GetUserAlbums(int) (*[]Album, error)
	GetUserTodos(int) (*[]Todo, error)
}
